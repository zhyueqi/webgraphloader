package lab.zh.graph.java;

import java.io.IOException;

import it.unimi.dsi.logging.ProgressLogger;
import it.unimi.dsi.webgraph.*;

public class TransFormat {
	//Print nodes
	final boolean print=false;
	//a progress logger used while loading the graph
	final ProgressLogger pl = new ProgressLogger();
	
	public static void main(String[] args) {
		new  TransFormat().loadSequencially("./file/amazon-2008-hc");
	}
	/*
	 * @Info load graph without random access ability
	 */
	public void loadSequencially(String path){
		try {
			//The time interval for a new log in milliseconds
			pl.logInterval = 100L;
			
			BVGraph graph = BVGraph.load(path,0,pl);
			final int n = graph.numNodes();
			
			pl.start( "Starting visit..." );
			//The number of expected calls to {@link #update()}
			pl.expectedUpdates = n-0;
			pl.itemsName = "nodes";
			
			NodeIterator nodeIterator = graph.nodeIterator();
			while(nodeIterator.hasNext()){
				int node =nodeIterator.nextInt();
				if(print)
					System.out.println("node"+node);
				LazyIntIterator lazyIterator = nodeIterator.successors();
				int outDegreeNode = 0;
				while((outDegreeNode = lazyIterator.nextInt())!=-1){
					if(print)
						System.out.println("----->"+outDegreeNode);
				}
				pl.update();
			}
			pl.done();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
